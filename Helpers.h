#pragma once

int squareOfSum(int a, int b)
{
	int sum = a + b;
	return sum * sum;
}